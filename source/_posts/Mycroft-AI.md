---
title: Mycroft AI
comments: false
categories:
  - Guide
aubot: Garheade
excerpt:
  - Simple summary of post
toc: false
date: 2023-02-16 11:13:53
tags: 
- Guide
- Open Source
- Mycroft AI
- Home Automation
cover: A quick guide on how I guilt a Mycroft AI.
---

## What I'm doing:

When I started college, I switched most of my devices over to Apple products. The integrations that they offer make life and a lot of other tracking of tasks and docs much easier for me. I miss using Linux on the desktop but the iPhone and Apple Watch are what every phone/smart watch should strive for. I was gifted some HomePods a while back and I like the ability to turn lights on/off and adjust temperatures, etc. All this tracking that is involved bothers me.

I've wanted to try building a [Mycroft AI](https://mycroft.ai/) for years but have never taken the time to make it happen, until last night. I wanted to buy a Mark II but the price has always been an issue. I would have built one on a Pi 4, but they are extremely hard to come by these days.

## Setting Up the Laptop

I have an old Lenovo laptop sitting around that was missing the N key. I decided that, with the built in camera, microphone, and speakers, it would be a perfect machine to run Mycroft on. I installed a standard Linux Mint XFCE edition on it and ran the typical updates. I confirmed that the camera, speakers, and microphone all worked and then reboot one final time.

## Installing Mycroft AI

Installing Mycroft AI is really easy to install. The [instructions](https://mycroft-ai.gitbook.io/docs/using-mycroft-ai/get-mycroft/linux) on the Mycroft AI website for linux are incredibly easy to follow.

1. Clone the git repo:
  ```bash
  git clone https://github.com/MycroftAI/mycroft-core.git
  ```
2. Move into the mycroft-core directory:
  ```
  cd mycroft-core
  ```
3. Now run the install script:
  `./dev_setup.sh`
4. Once the install was complete, just run:
  `./start-mycroft.sh all`

Boom! Now you can talk to Mycroft!

Just a quick fun project, I hope this helps, somehow.

-G


<a rel="me" href="https://mstdn.plus/@garheade">Mastodon</a>
