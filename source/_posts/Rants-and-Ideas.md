---
title: Rants and Ideas
date: 2023-02-12 18:50:23
tags:
- Ideas
- Opinions
- Plans
- Linux
- Youtube 
---

## Where I'm at in life.

For those who don't know me personally, I have been working full-time as a systems administrator for more than 15 years. I've worked for transit companies, medical companies, colleges, and a scientific research non-profit. All those years, I have followed and implemented open-source software everywhere I can. Also, in the middle of the Covid Pandemic, I returned to school and am working toward my BS in Computer Information Systems. 

Over the last ten years, I have also worked with a group called the [Software Freedom School](www.sofree.us). SFS aims to teach the world "Why to choose, and how to use libre software." At this point, we've taught hundreds of students how to use various open-source software and operating systems.

I have recently started to get overloaded with work. My volunteer time at SFS has diminished as my full-time job, and school become more demanding. I have also started having issues with celebrating people that say and do horrible and inhuman things just because they have done something they like to use. I also feel more like an employee than a leader and teacher within the school. All these are leading me to seek out other possible projects or ways to share

## So What is Next

As both a systems administrator and a teacher, one of the most amazing resources these days is YouTube. Unfortunately, while it is a great resource, it could translate better for use as a guide. I have been caught scrolling back and forth through videos because referencing specific parts is almost impossible.

I am going to start transcribing some of these videos into valuable guides. It will be slow to start, but I plan to build a decent library over the coming months. Please let me know if you know of good videos I should transcribe/transform into a written guide.


<a rel="me" href="https://mstdn.plus/@garheade">Mastodon</a>
