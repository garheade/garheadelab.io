---
title: About
date: 2023-02-12
aubot: Garheade
describe: 'Hi, this is me! I am real human and I like breathing very much'
type: "about"
layout: "about"
comments: false
---

## My Active Projects and Goals

1. Complete by Degree
2. Learn and Improve my Tasks at work
3. LFCA Certificate
4. LFCS Certificate
5. Help [SFS](www.sofree.us) as much as I can.

## Active Certs

<div data-iframe-width="150" data-iframe-height="270" data-share-badge-id="ace3e0af-b243-4c88-9e41-03af4fa1930b" data-share-badge-host="https://www.credly.com"></div><script type="text/javascript" async src="//cdn.credly.com/assets/utilities/embed.js"></script>

## Resume

### Coming Soon ###

